import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CalculatorScreen extends StatefulWidget {
  @override
  _CalculatorScreenState createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {

  String value = "0";
  String operationalValue = "0";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                      padding: EdgeInsets.all(20.0),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          value,
                          style: TextStyle(
                            fontSize: 30.0,
                          ),
                        ),
                      ),
                  ),
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: calcBtns('C',0xfff2e6d9),
                onTap: (){
                  setState(() {
                    value = "0";
                    operationalValue = value;
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('Del',0xfff2e6d9),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value = value.substring(0,value.length-1);
                      if(value==""){
                        value = "0";
                        operationalValue = value;
                      }
                    }else{
                      value = "0";
                      operationalValue = value;
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('%',0xfff2e6d9),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      var myDouble = double.parse(value);
                      myDouble = myDouble/100;
                      value = myDouble.toString();
                      operationalValue = value;
                    }else{
                      value = "0";
                      operationalValue = value;
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('+',0xfff2e6d9),
                onTap: (){
                  setState(() {
                    operationalValue += "+";
                  });
                },
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: calcBtns('7',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "7";
                      operationalValue += "7";
                      if(operationalValue.contains('+')){
                        value = addTwoNumbers(operationalValue);
                        operationalValue = value;
                      }
                    }else{
                      value = "7";
                      operationalValue = value;
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('8',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "8";
                    }else{
                      value = "8";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('9',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "9";
                    }else{
                      value = "9";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('x',0xfff2e6d9),
                onTap: (){
                  setState(() {

                  });
                },
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: calcBtns('4',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "4";
                    }else{
                      value = "4";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('5',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "5";
                    }else{
                      value = "5";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('6',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "6";
                    }else{
                      value = "6";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('+',0xfff2e6d9),
                onTap: (){
                  setState(() {

                  });
                },
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: calcBtns('1',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "1";
                    }else{
                      value = "1";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('2',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "2";
                    }else{
                      value = "2";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('3',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "3";
                    }else{
                      value = "3";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('-',0xfff2e6d9),
                onTap: (){
                  setState(() {

                  });
                },
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: calcBtnWide('0',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    if(value!="0"){
                      value += "0";
                    }else{
                      value = "0";
                    }
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('.',0xffe0e0eb),
                onTap: (){
                  setState(() {
                    value += ".";
                    operationalValue = value;
                  });
                },
              ),
              GestureDetector(
                child: calcBtns('=',0xffffce99),
                onTap: (){
                  setState(() {

                  });
                },
              ),
            ],
          ),
          SizedBox(height: 10.0),
        ],
      ),
    );
  }

  Widget calcBtns(String value, int color){
    return Container(
      decoration: BoxDecoration(
        color: Color(color),
        borderRadius: BorderRadius.circular(10.0),
      ),
      width: MediaQuery.of(context).size.width/5,
      height: 50,
      child: Center(
        child: Text(
          value,
        ),
      ),
    );
  }

  Widget calcBtnWide(String value, int color){
    return Container(
      decoration: BoxDecoration(
        color: Color(color),
        borderRadius: BorderRadius.circular(10.0),
      ),
      width: 160.0,
      height: 50,
      child: Center(
        child: Text(
          value,
        ),
      ),
    );
  }

  addTwoNumbers(value){
    List<String> fullString = value.split("+");
    String firstNumber = fullString[0];
    String secondNumber = fullString[1];
    if(value.contains('.')){
      double result = double.parse(firstNumber) + double.parse(secondNumber);
      value = result.toString();
      return value;
    }
    else{
      int result = int.parse(firstNumber) + int.parse(secondNumber);
      value = result.toString();
      return value;
    }
  }
}
